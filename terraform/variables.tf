variable "profile" {
  default = "default"
}

variable "region" {
  default = "us-east-1"
}

variable "instance" {
  default = "t2.small"
}

variable "instance_count" {
  default = "1"
}

variable "public_key" {
  default = "../MyKey.pub"
}

variable "private_key" {
  default = "../MyKey.pem"
}

variable "ansible_user" {
  default = "ubuntu"
}

variable "ami" {
  default = "ami-0e472ba40eb589f49"
}
