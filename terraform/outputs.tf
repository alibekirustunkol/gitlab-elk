output "kibana" {
  value = "http://${aws_instance.kibana.0.public_ip}:5601"
  
}

output "elastic" {
  value = "http://${aws_instance.elastic.0.public_ip}:9200"
}

output "application" {
  value = "http://${aws_instance.application.0.public_ip}:8080"
}

resource "local_file" "elastic-ip" {
  depends_on   = [aws_instance.elastic]
  filename     = "elastic-ip"
  content      = aws_instance.elastic.0.public_ip
}

resource "local_file" "kibana-ip" {
  depends_on   = [aws_instance.kibana]
  filename     = "kibana-ip"
  content      = aws_instance.kibana.0.public_ip
}

resource "local_file" "application-ip" {
  depends_on   = [aws_instance.application]
  filename     = "application-ip"
  content      = aws_instance.application.0.public_ip
}
